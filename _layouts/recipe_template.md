---
layout: page
title:  Receipe template
tags: [recipe]
---

# Section

## Summary

- YIELD: Makes about 8 5-ounce servings
- ACTIVE TIME: 30 minutes
- TOTAL TIME: 30 minutes plus 4 hours steeping
- REFERENCES:
  - [link]()

## Ingredients

1. for source
	1. 28 ounces milk
	1. 1 empty vanilla bean pod
1. 16 ounces peeled, ripe banana

## Directions

1. A
1. B
1. C

## Tips

- A
- B
- C