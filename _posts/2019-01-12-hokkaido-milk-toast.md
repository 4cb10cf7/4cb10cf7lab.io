---
layout: page
title:  "Hokkaido Milk Toast"
tags: [recipe]
---

# Hokkaido Milk Toast

## Summary

- YIELD: 吐司模具 * 1
- ACTIVE TIME: 60 minutes
- TOTAL TIME: 4 hours + 4 hours
- REFERENCES:
  - [北海道牛奶麵包【湯種法】Hokkaido Milk Toast](https://www.christinesrecipes.com/2010/10/hokkaido-milk-toast.html)
  - [何謂湯種法麵糰](https://caroleasylife.blogspot.com/2009/02/blog-post_28.html)

## Ingredients

1. 干
	1. 强力粉 270g
	1. 糖 80g
	1. 奶粉 20g
	1. 汤种 80g
	1. 酵母 5g
	1. 泡打粉 5g
2. 湿
	1. 全蛋 1个
	1. 淡奶油 70g
3. 油盐
	1. 盐 2g
	1. 黄油 70g 切小块

## Directions

1. 混合#1
1. 混合#2, 加入#1=>#4
1. 搅拌至光滑 速度2->6 ~10 mins
1. 混合#3, 加入#4=>#5
1. 搅拌至手套膜 速度2->6->2 ~20 mins
1. 烤箱中层29度发酵 90 mins 2倍大
1. 排气, 分成3份, 揉成团
1. 加保鲜膜, 醒发 15 mins
1. 排气, 擀成长条, 卷起, 排气；另一个方向擀成长条, 卷起；放入吐司模具, 收口朝下
1. 烤箱中层29度发酵 90 mins 8分满
1. 烤箱中层上下火175度 35 mins
1. 脱模, 测放, 放凉

## Tips

- 搅拌每5 mins检查一次, 可再加水
