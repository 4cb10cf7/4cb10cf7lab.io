---
layout: page
title: Recipes
permalink: /recipes.html
---

{% for post in site.posts %}
	{% if post.tags contains "recipe" %}
- [{{ post.title }}]({{ post.url }})
	{% endif %}
 {% endfor %}
